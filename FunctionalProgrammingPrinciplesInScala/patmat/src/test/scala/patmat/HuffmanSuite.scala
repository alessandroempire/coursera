package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
    val t0 = Leaf('x', 8)
    val t01 = Leaf('y', 2)
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }


  test("weight of a Leaf") {
    new TestTrees {
      assert(weight(t0) === 8)
    }
  }


  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }


  test("chars of a Leaf") {
    new TestTrees {
      assert(chars(t0) === List('x'))
    }
  }


  test("times (hello world) "){
    assert(times("hello world".toList) === List(('e',1), (' ', 1), ('l', 3), ('h',1), ('r',1), ('w', 1),
      ('o', 2), ('d', 1)))
  }


  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }


  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("makeOrderedLeafList for some frequency table 2") {
    assert(makeOrderedLeafList(List(('a', 2), ('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('a', 2), Leaf('t',2), Leaf('x',3)))
  }


  test("singleton with empty list"){
    assert(singleton(List()) === false)
  }


  test("singleton with one Code tree"){
    new TestTrees {
      assert(singleton(List(t0)) === true)
    }
  }


  test("singleton with more than one Code tree"){
    new TestTrees {
      assert(singleton(List(t01, t0)) === false)
    }
  }


  test("combine for a list with just a leaf"){
    new TestTrees {
      assert(combine(List(t0)) === List(t0))
    }
  }


  test("combine for a list with just a two leaf"){
    new TestTrees {
      val fork = Fork(Leaf('x', 8), Leaf('y', 2), List('x','y'), 10)
      assert(combine(List(t0, t01)) === List(fork))
    }
  }


  test("combine of some leaf list") {
    val leafList = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leafList) === List( Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }


  test("combine of some leaf list 2") {
    val leafList = List(Leaf('e', 5), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leafList) === List( Leaf('x',4), Fork(Leaf('e',5),Leaf('t',2),List('e', 't'),7)))
  }


  test("combine of some leaf list 3") {
    val chars = "xxxxyeeeeett".toList
    val leafList = makeOrderedLeafList(times(chars))
    assert(combine(leafList) === List( Fork(Leaf('t',2), Leaf('y', 1), List('t', 'y'), 3), Leaf('x', 4), Leaf('e', 5) ))
  }


  test("until of some leaf list"){
    val chars = "ettxxxx".toList
    val leafList = makeOrderedLeafList(times(chars))
    val finalTree = Fork( Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4), List('e', 't', 'x'), 7)

    assert( until(singleton, combine)(leafList) === finalTree)
  }


  /**
    *
    * Second part of the assingment
    *
    */


  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }


  test("create code tree"){
    new TestTrees {
      val myString = "ababb"
      assert(createCodeTree(myString.toList) === t1)
    }

  }


  test("enconde simple string ab") {
    new TestTrees {
      assert(encode(t1)("ab".toList) === List(0,1))
    }
  }


  test("enconde simple string ab with border case of char that is not known") {
    new TestTrees {
      assert(encode(t1)("abc".toList) === List(0,1))
    }
  }


  test("decode simple List of bits ") {
    new TestTrees {
      assert(decode(t1, List(0,1)) === List('a','b'))
    }
  }

  test("decode the example in the page ") {
    val chars = "aaaaaaaabbbcdefgh".toList
    val tree = createCodeTree(chars)

    assert(decode(tree, List(1,0,0,0,1,0,1,0)) === List('b', 'a', 'c'))
  }


  test("convert tree t1 to a code table"){
    new TestTrees {
      val codeTable: CodeTable = List(('a', List(0)), ('b', List(1)))
      assert(convert(t1) === codeTable)
    }
  }


  test("convert tree t2 to a code table"){
    new TestTrees {
      val codeTable: CodeTable = List(('a', List(0,0)), ('b', List(0,1)), ('d', List(1)))
      assert(convert(t2) === codeTable)
    }
  }


  test("code bits a tree to a code table"){
    new TestTrees {
      val codeTable: CodeTable = List(('a', List(0)), ('b', List(1)))
      assert(codeBits(codeTable)('b') === List(1))

    }
  }


  test("Quick encode simple string ab") {
    new TestTrees {
      assert(quickEncode(t1)("ab".toList) === List(0,1))
    }
  }


  test("Quick Encode of example ") {
    val chars = "aaaaaaaabbbcdefgh".toList
    val tree = createCodeTree(chars)
    val codeTable: CodeTable = List(
      ('a', List(0)),
      ('b', List(1,0,0)),
      ('c', List(1,0,1,0)),
      ('d', List(1,0,1,1)),
      ('e', List(1,1,0,0)),
      ('f', List(1,1,0,1)),
      ('g', List(1,1,1,0)),
      ('h', List(1,1,1,1)))

    assert( quickEncode(tree)("b".toList) === codeBits(codeTable)('b'))
    assert( quickEncode(tree)("d".toList) === codeBits(codeTable)('d'))
    assert( quickEncode(tree)("e".toList) === codeBits(codeTable)('e'))
    assert( quickEncode(tree)("h".toList) === codeBits(codeTable)('h'))
    assert( quickEncode(tree)("de".toList) === codeBits(codeTable)('d') ++ codeBits(codeTable)('e'))
  }


  test("decode and Quick encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
    }
  }
}
