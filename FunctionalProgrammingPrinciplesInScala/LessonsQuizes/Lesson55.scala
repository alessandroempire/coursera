

def mapFun[T, U](xs: List[T], f: T => U): List[U] =
  (xs foldRight List[U]())( (x, acc) => f x :: acc )


def lengthFun[T](xs: List[T]): Int =
  (xs foldRight 0)( (_, acc) => acc + 1 )

