
/**
* 
*/

def isSort(xs: List[Int]): List[Int] = xs match {
    case Nil => List()
    case head :: tail => insert(head, isSort(tail))
}

def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case Nil => List(x)
    case head :: tail => 
        if (x <= head ) x :: xs  // or x :: head :: tail
        else head :: insert(x, tail)
}








List(7,3,2,5)

7; 3,2,5
3; 2,5
2; 5
5; Nil

5 :: Nil
2 :: 5 :: Nil

