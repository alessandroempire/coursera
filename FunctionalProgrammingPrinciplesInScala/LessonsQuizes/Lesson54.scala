
/** Me */

def squareList(xs: List[Int]): List[Int] = {
    xs match {
        case Nil => Nil
        case x :: xs1 => x*x :: squareList(xs1)
    }
}

def squareList(xs: List[Int]): List[Int] = {
    xs map (x => x*x)
}



/* me */

def pack[T] (xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 => 
        xs1 takeWhile (y => y == x) :: pack( xs1 dropWhile (y => y != x) )
}

def pack[T] (xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 => 
        val (first, rest) = xs1 span (y => y == x)
        first :: pack(rest)
}


/* Me */

def encode[T](xs: List[T]): List[(T,Int)] = {
    pack(xs) map( t => (t.head, t.length))
}

