

/**
*/
def init[T](xs: List[T]): List[T] = {

    def initRecursive[T](xs: List[T], acc: List[T]): List[T] = xs match {
        case Nil => throw Error("init of empty list")
        case x :: Nil => acc
        case x :: xs => (xs, x :: acc)
    }

    initRecursive(xs, List())
}


def init[T](xs: List[T]): List[T] = xs match {
    case Nil => throw Error("init of empty list")
    case x :: Nil => List()
    case x :: xs => x :: init(xs)
}


/** Me */
def concat[T](xs: List[T], ys: List[T]): List[T] = ys match {
    case Nil => xs
    case z :: zs => (xs ++ z, zs)
}

def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case Nil => ys
    case z :: zs => z :: concat(zs, ys)
}




/** me */
def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => xs 
    case y :: ys => reverse(ys) ++ List(y)
}


/** me */
def removeAt[T](n: Int, xs: List[T]): List[T] = xs match {
    case List() => xs
    case y :: ys if (n == 0) => ys
    case y :: ys  => y :: remove(n-1, ys)
}

def removeAt[T](n: Int, xs: List[T]): List[T] = (xs take n) ::: (x drop n + 1)


def flatten