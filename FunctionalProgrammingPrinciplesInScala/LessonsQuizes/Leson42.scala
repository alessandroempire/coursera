
trait List[T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]
}

class Nil[t] extends List[T] {
    def isEmpty: Boolean = true

    def head: Nothing = throw new NoSuchElementExcetion

    def tail: Nothing = throw new NoSuchElementExcetion
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty: Boolean = false
}

object List{

    def apply[T](): List[T] = new Nil

    def apply[T](x: T): List[T] = new Cons(x, new Nil)

    def apply[T](x1: T, x2: T): List[T] = new Cons(x1, new Cons(x2, new Nil))
}