


/*
* Implement < in class Boolean
* assume false < true
*/

def < (x: Boolean) = ifThenElse(false, x)


/**
* Natural numbers Quiz
*
*/

abstract class Nat {
    def isZero: Boolean
    def predecesor: Nat
    def successor: Nat =  new Succ(this) //Succ of zero would be 1. 
    def + (that: Nat): Nat
    def - (that: Nat): Nat
}


object zero extends Nat{
    def isZero: Boolean = true
    def predecesor: Nat = throw new Error
    def + (that: Nat): Nat = that
    def - (that: Nat): Nat = if (that.isZero) this else throw new Error
}

/**
* Represents the class that is one bigger than n. 
*/
class Succ(n: Nat) extends Nat {
    def isZero: Boolean = false
    def predecesor: Nat = n
    def + (that: Nat): Nat = new Succ(n + that) 
    def - (that: Nat): Nat = if (that.isZero) that else n - that.predecesor //
}




