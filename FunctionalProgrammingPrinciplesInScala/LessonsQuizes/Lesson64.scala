


class Poly(terms0: Map[Int, Double]) {

    def this(bindings: (Int, Double)*) = this(bindings.toMap)  //the * == Seq

    val terms = terms0 withDefaultValue 0.0

    def + (other: Poly) = new Poly(terms ++ (others.terms map adjust))

    def adjust(term: (Int, Double)): (Int, Double) {
        val (exp, coeff) = term
        exp -> (coeff + terms(exp))
    }

    override def toString = (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp) mkString " + "

}


/** With fold left */

class Poly(terms0: Map[Int, Double]) {

    def this(bindings: (Int, Double)*) = this(bindings.toMap)  //the * == Seq

    val terms = terms0 withDefaultValue 0.0

    def + (other: Poly) = new Poly(other.terms foldLeft (terms)(addTerm))
    /**
    Instead of Map[(Int,Double)]() and create a new one in memory, use the one in memmory and only
    change the pointers we need to change. 
    */

    def addTerm(acc: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
        val (exp, coeff) = term
        acc + (exp -> coeff + term(exp)) //Tuple
    }


    override def toString = (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp) mkString " + "

}