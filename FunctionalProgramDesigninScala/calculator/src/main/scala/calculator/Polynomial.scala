package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal{
      val aa: Double = a()
      val bb: Double = b()
      val cc: Double = c()
      Math.pow(bb,2) - 4*aa*cc
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal{
      val aa: Double = a()
      val bb: Double = b()
      val cc: Double = c()
      val dd: Double = delta()
      val plus = (-bb + Math.sqrt(dd)) / 2*aa
      val minus = (-bb - Math.sqrt(dd)) / 2*aa
      Set(minus, plus).filterNot(_.isNaN)
    }
  }
}
