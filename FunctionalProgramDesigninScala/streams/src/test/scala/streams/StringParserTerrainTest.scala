package streams

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringParserTerrainTest extends FunSuite {


  test("terrainFunction normal and border cases") {
    new StringParserTerrain {
      /**
        * A ASCII representation of the terrain. This field should remain
        * abstract here.
        */
      override val level: String =
        """SX
           |oo
           |oo""".stripMargin

      val func = terrainFunction(Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'o')))
      assert(func(new Pos(0,0)))
      assert(func(new Pos(0,1)))
      assert(!func(new Pos(0,2)))
      assert(func(new Pos(2,1)))
      assert(!func(new Pos(2,2)))
    }
  }


  test("findChar normal and border cases") {
    new StringParserTerrain {
      /**
        * A ASCII representation of the terrain. This field should remain
        * abstract here.
        */
      override val level: String =
        """SX
          |oo
          |oo""".stripMargin

      val vector = Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'o'))
      assert(findChar('S', vector) === Pos(0,0))
      assert(findChar('T', vector) === Pos(0,1))
    }
  }


  test("Gamedef legal Neighbors is empty") {
    new StringParserTerrain {
      /**
        * A ASCII representation of the terrain. This field should remain
        * abstract here.
        */
      override val level: String =
        """oo
          |ST
          |oo""".stripMargin.replaceAll("\r", "")

      val legalNeighbors: List[(Block, Move)] = startBlock.legalNeighbors
      assert(legalNeighbors.isEmpty)
    }
  }

  test("Gamedef legal Neighbors is not empty") {
    new StringParserTerrain {
      /**
        * A ASCII representation of the terrain. This field should remain
        * abstract here.
        */
      override val level: String =
        """ooo
          |ooo
          |oSTo
          |ooo
          |ooo""".stripMargin.replaceAll("\r", "")

      val legalNeighbors: List[(Block, Move)] = startBlock.legalNeighbors

      assert(legalNeighbors(0) === (Block(Pos(3,1), Pos(4,1)), Down))
      assert(legalNeighbors(1) === (Block(Pos(0,1), Pos(1,1)), Up))
      assert(legalNeighbors(2) === (Block(Pos(2,2), Pos(2,3)), Right))
    }
  }
}
