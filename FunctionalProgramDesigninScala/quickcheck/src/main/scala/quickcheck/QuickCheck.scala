package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  /**
    * Heap Generator
    */
  lazy val genHeap: Gen[H] = for{
    elem <- Arbitrary.arbitrary[Int]
    heap <- Gen.oneOf(Gen.const(QuickCheckHeap.this.empty), genHeap)
  } yield insert(elem, heap)


  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)


  /**
    * adding a single element to an empty heap, and then removing this element,
    * should yield the element in question.
    */
  property("min1") = forAll{ a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  /**
    *  Adding the minimal element, and then finding it,
    *  should return the element in question
    */
  property("gen1") = forAll{ (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m,h)) == m
  }

  /**
    * If you insert any two elements into an empty heap,
    * finding the minimum of the resulting heap should get
    * the smallest of the two elements back.
    */
  property("insert2Elems") = forAll{ (e1: Int, e2: Int) =>
    val h = insert(e2, insert(e1, empty))
    findMin(h) == Math.min(e1,e2)
  }

  /**
    * If you insert an element into an empty heap, then delete the minimum,
    * the resulting heap should be empty.
    */
  property("insertAndDeleteElemInEmptyHeap") = forAll { e: Int =>
    val h = insert(e, empty)
    deleteMin(h) == empty
  }

  /**
    * Given any heap, you should get a sorted sequence of elements when
    * continually finding and deleting minima.
    * (Hint: recursion and helper functions are your friends.)
    */
  property("sortedSequenceOfElems") = forAll{ h: H =>
    def elem(heap: H, list: List[A]): List[A] =
      if (isEmpty(heap)) list
      else {
        val e = findMin(heap)
        elem(deleteMin(heap), e :: list)
      }

    val xs = elem(h, List()).reverse
    xs == xs.sorted
  }

  /**
    * Finding a minimum of the melding of any two heaps should return a
    * minimum of one or the other.
    */
  property("minMeldingTwoHeaps") = forAll{ (h1: H, h2: H) =>
    val min1 = findMin(h1)
    val min2 = findMin(h2)
    val meldH = meld(h1, h2)
    findMin(meldH) == Math.min(min1, min2)
  }


  /**
    * If we insert two elements and remove the minima, then finding the minima of the new heap
    * should be equal to the max of the two elements
    */
  property("heapMaxWhenRemovingMin") = forAll{ (e1: Int, e2: Int) =>
    val h1 = insert(e2, insert(e1, empty))
    val hMax = deleteMin(h1)
    findMin(hMax) == Math.max(e1, e2)
  }


  /**
    * Same as above but with 3 elements.
    */
  property("insert3AndDeleting2ShouldGiveMax") = forAll { (e1: Int, e2: Int, e3: Int) =>
    val h1 = insert(e1, insert(e2, insert(e3, empty)))
    val h2 = deleteMin(deleteMin(h1))
    findMin(h2) == Math.max(e1, Math.max(e2, e3))
  }

}
