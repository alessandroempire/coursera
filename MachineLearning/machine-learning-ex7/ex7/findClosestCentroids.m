function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);


%Declre variables
min_distance = 0; 
c = 0; 
index = -1; 
m = size(X,1);
K = size(centroids,1)


for i = 1:m
	min_distance = Inf; 
	for j = 1:K
		matrix = abs(X(i,:) - centroids(j,:));
		c = matrix * matrix'; 					%'
		if c < min_distance
			min_distance = c; 
			index = j; 
		endif
	endfor
	idx(i) = index; 
endfor


% =============================================================

end

