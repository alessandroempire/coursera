package stackoverflow

import org.scalatest.{FunSuite, BeforeAndAfterAll}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.spark.rdd.RDD

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {


  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")
    override def langSpread = 50000
    override def kmeansKernels = 45
    override def kmeansEta: Double = 20.0D
    override def kmeansMaxIterations = 120
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  trait data {
    val file = List(
      "1,100,,,9,Scala",
      "2,101,,100,1,Scala",
      "2,102,,100,7,Scala",
      "1,103,,,5,Java",
      "2,104,,103,8,Java",
      "1,105,,,1,Python"
    )
    import StackOverflow._
    val rdd = sc.parallelize(file)
  }

  test("groupedPostings"){
    new data {
      val rawPostings = StackOverflow.rawPostings(rdd)
      val groupedPostings = StackOverflow.groupedPostings(rawPostings)
      val collectedResult = groupedPostings.collect()
      val firstGrouping = collectedResult(0)._2.toArray

      assert( collectedResult(0)._1 === 100)
      assert( collectedResult(0)._2.size === 2)
      assert( firstGrouping(0)._1.id === 100)
      assert( firstGrouping(0)._2.id === 101)
      assert( firstGrouping(0)._2.parentId.get === 100)
      assert( collectedResult(1)._1 === 103)
      assert( collectedResult(1)._2.size === 1)
    }
  }

  test("scoredPostings"){
    new data {
      val rawPostings = StackOverflow.rawPostings(rdd)
      val groupedPostings = StackOverflow.groupedPostings(rawPostings)
      val scoredPostings = StackOverflow.scoredPostings(groupedPostings)
      val collectedResult = scoredPostings.collect()

      assert( collectedResult(0)._1.id === 100 )
      assert( collectedResult(0)._2 === 7 )
      assert( collectedResult(1)._1.id === 103 )
      assert( collectedResult(1)._2 === 8 )
    }
  }

  test("scored number of entries"){
    import StackOverflow._
    val lines: RDD[String]   = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")
    val raw: RDD[Posting]     = rawPostings(lines)
    val grouped: RDD[(QID, Iterable[(Question, Answer)])] = groupedPostings(raw)
    val scored  = scoredPostings(grouped)

    assert ( scored.count() === 2121822 )
  }

  test("vectorPostings"){
    new data {
      val rawPostings = StackOverflow.rawPostings(rdd)
      val groupedPostings = StackOverflow.groupedPostings(rawPostings)
      val scoredPostings = StackOverflow.scoredPostings(groupedPostings)
      val vectorPostings = StackOverflow.vectorPostings(scoredPostings)
      val collectedResult = vectorPostings.collect()

      assert( collectedResult(0)._1 === 10*StackOverflow.langSpread) //Scala is the 11th element, 11-1 (it start counting from zero)
      assert( collectedResult(0)._2 === 7)
      assert( collectedResult(1)._1 === 1*StackOverflow.langSpread) //Java is the second element, 2-1
      assert( collectedResult(1)._2 === 8)
    }
  }

}
