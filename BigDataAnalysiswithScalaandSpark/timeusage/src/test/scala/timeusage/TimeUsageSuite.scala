package timeusage

import org.apache.spark.sql.{Column, ColumnName, DataFrame, Row}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import timeusage.TimeUsage.{age, classifiedColumns, other, primaryNeeds, read, timeUsageGrouped, timeUsageGroupedSql, timeUsageSummary, work, workingStatus}

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TimeUsageSuite extends FunSuite with BeforeAndAfterAll {

  test("dfSchema should infer the proper schema"){
    val list = List("a", "b")

    val expectedResult: StructType = StructType(
      Seq(
        StructField("a", StringType, false),
        StructField("b", DoubleType, false)
      )
    )

    assert(TimeUsage.dfSchema(list) === expectedResult)
  }

  test("row should transform a list to a row of first string followed by doubles"){
    val list = List("a", "1")
    val expectedResult: Row = Row("a", 1.0)

    assert(expectedResult === TimeUsage.row(list))
  }

  test("classifiedColumns should classify columns correctly"){
    val list = List("t01", "t03", "t11", "t1801", "t1803", "t05", "t1805", "t02", "t04", "t06", "t07", "t08", "t09",
      "t10", "t12", "t13", "t14", "t15", "t16", "t18", "randomColumn")

    val primaryNeeds = List(col("t01"), col("t03"), col("t11"), col("t1801"),
      col("t1803"))
    val workingActivities = List(col("t05"), col("t1805"))
    val otherActivities = List(col("t02"), col("t04"), col("t06"), col("t07"),
      col("t08"), col("t09"), col("t10"), col("t12"), col("t13"),
      col("t14"), col("t15"), col("t16"), col("t18"))

    assert((primaryNeeds, workingActivities, otherActivities) === TimeUsage.classifiedColumns(list))
  }


  test("timeUsageGroupedSqlQuery should equal timeUsageGrouped"){
    val (columns, initDf) = read("/timeusage/atussum.csv")
    val (primaryNeedsColumns, workColumns, otherColumns) = classifiedColumns(columns)
    val summaryDf = timeUsageSummary(primaryNeedsColumns, workColumns, otherColumns, initDf)
    summaryDf.cache()

    val finalDf = timeUsageGrouped(summaryDf).cache()
    val timeUsageGroupedSqlVal = timeUsageGroupedSql(summaryDf).cache()

    val finalDFRows = finalDf.collect().map(transformer(_))
      .sortBy(t => (t.working, t.sex, t.age, t.work, t.other, t.primaryNeeds))
    val timeUsageGroupedSqlValRows = timeUsageGroupedSqlVal.collect().map(transformer(_))
      .sortBy(t => (t.working, t.sex, t.age, t.work, t.other, t.primaryNeeds))

    assert(timeUsageGroupedSqlVal.count() === finalDf.count())
    assert(finalDFRows === timeUsageGroupedSqlValRows)
  }


  def transformer(row: Row): TimeUsageRow = {
    TimeUsageRow(
      row.getAs[String](workingStatus),
      row.getAs[String](age),
      row.getAs[String](age),
      row.getAs[Double](primaryNeeds),
      row.getAs[Double](work),
      row.getAs[Double](other)
    )
  }


  test("timeUsageGroupedTypedVal should equal timeUsageGrouped"){
    val (columns, initDf) = read("/timeusage/atussum.csv")
    val (primaryNeedsColumns, workColumns, otherColumns) = classifiedColumns(columns)
    val summaryDf = timeUsageSummary(primaryNeedsColumns, workColumns, otherColumns, initDf)
    summaryDf.cache()

    val finalDf = timeUsageGrouped(summaryDf).cache()
    val timeUsageSummaryTypedVal = TimeUsage.timeUsageSummaryTyped(summaryDf)
    val timeUsageGroupedTypedVal = TimeUsage.timeUsageGroupedTyped(timeUsageSummaryTypedVal).cache()

    assert(timeUsageGroupedTypedVal.count() === finalDf.count())
  }

}
