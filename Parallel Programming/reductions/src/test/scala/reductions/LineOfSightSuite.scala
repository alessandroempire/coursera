package reductions

import java.util.concurrent._
import scala.collection._
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import common._
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory

@RunWith(classOf[JUnitRunner]) 
class LineOfSightSuite extends FunSuite {

  import LineOfSight._

  /**
    *
    */
  test("lineOfSight should correctly handle an array of size 4") {
    val output = new Array[Float](4)
    lineOfSight(Array[Float](0f, 1f, 8f, 9f), output)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

  test("parlineOfSight should correctly handle an array of size 4 par") {
    val output = new Array[Float](4)
    parLineOfSight(Array[Float](0f, 1f, 8f, 9f), output, 1)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

  /**
    * Upsweep
    */
  test("upsweepSequential should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f), 1, 4)
    assert(res == 4f)
  }

//  test("upsweepSequential should correctly handle the chunk 0 until 1 of an array of 4 elements") {
//    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f), 0, 1)
//    assert(res == 0f)
//  }

//  test("upsweep") {
//    val input = Array[Float](0f, 1f, 8f, 9f)
//    val res: Tree = upsweep(input, 0, input.length, 1)
//    val tree = Node(Node(Leaf(0,1,0f),Leaf(1,2,1f)),Node(Leaf(2,3,4f),Leaf(3,4,3f)))
//    assert(tree === res)
//  }


  /**
    * Downseep
    */
  test("downsweepSequential should correctly handle a 4 element array when the starting angle is zero") {
    val output = new Array[Float](4)
    downsweepSequential(Array[Float](0f, 1f, 8f, 9f), output, 0f, 1, 4)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

}

